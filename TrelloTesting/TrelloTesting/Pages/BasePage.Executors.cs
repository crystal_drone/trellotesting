﻿using ExamPlanner.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace TrelloTesting.Pages
{
    public abstract partial class BasePage
    {
        public IWebDriver driver = SeleniumExecutor.Driver;
        public WebDriverWait wait = new WebDriverWait(SeleniumExecutor.Driver, TimeSpan.FromSeconds(1));
    }
}
