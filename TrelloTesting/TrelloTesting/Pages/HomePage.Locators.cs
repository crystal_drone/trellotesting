﻿using OpenQA.Selenium;

namespace TrelloTesting.Pages
{
    public partial class HomePage
    {
        private By LoginButton => By.XPath("//a[@class='btn btn-sm btn-link text-white']");

        private By RegisterButton => By.XPath("//a[@class='btn btn-sm bg-white font-weight-bold']");
    }
}
