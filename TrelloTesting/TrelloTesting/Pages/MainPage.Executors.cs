﻿using ExamPlanner.Utilities;
using OpenQA.Selenium.Support.UI;
using System;

namespace TrelloTesting.Pages
{
    public partial class MainPage : BasePage
    {
        public bool IsUserLogged()
        {
            try
            {
                wait.Until(ExpectedConditions.ElementExists(this.WelcomeButton));

                var element = driver.FindElement(this.WelcomeButton);
                return element.Displayed && element.Enabled;
            }
            catch
            {
                return false;
            }
        }
    }
}
