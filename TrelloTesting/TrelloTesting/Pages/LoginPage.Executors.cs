﻿using ExamPlanner.Utilities;

namespace TrelloTesting.Pages
{
    public partial class LoginPage : BasePage
    {
        public void TypeCredentials(string email, string password)
        {
            driver.FindElement(this.UserInput).SendKeys(email);
            driver.FindElement(this.PasswordInput).SendKeys(password);
        }

        public void SubmitLoginCredentials() => driver.FindElement(this.LoginButton).Click();
    }
}
