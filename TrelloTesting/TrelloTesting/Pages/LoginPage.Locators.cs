﻿using OpenQA.Selenium;

namespace TrelloTesting.Pages
{
    public partial class LoginPage
    {
        private By UserInput => By.Id("user");

        private By PasswordInput => By.Id("password");

        private By LoginButton => By.Id("login");
    }
}
