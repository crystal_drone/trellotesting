﻿using ExamPlanner.Utilities;

namespace TrelloTesting.Pages
{
    public partial class HomePage : BasePage
    {
        public void ClickLoginButton() => driver.FindElement(this.LoginButton).Click();

        public void ClickRegisterButton() => driver.FindElement(this.RegisterButton).Click();

        public void GoToHomePage() => driver.Navigate().GoToUrl("https://trello.com/");
    }
}
