﻿using ExamPlanner.Utilities;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace TrelloTesting.Tests.Hooks
{
    [Binding]
    public class SpecflowHooks
    {
        [AfterScenario]
        public void AfterScenario()
        {
            SeleniumExecutor.Driver.Manage().Cookies.DeleteAllCookies();
            SeleniumExecutor.Close();
        }
    }
}
