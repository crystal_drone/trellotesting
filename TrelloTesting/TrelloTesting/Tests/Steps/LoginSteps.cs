﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using TrelloTesting.Pages;

namespace TrelloTesting.Tests.Steps
{
    [Binding]
     public class LoginSteps : BasePage
    {
        private HomePage homePage = new HomePage();
        private LoginPage loginPage = new LoginPage();
        private MainPage mainPage = new MainPage();

        [Given(@"I open Trello home page")]
        public void GivenIOpenTrelloHomePage()
        {
            this.homePage.GoToHomePage();
        }

        [Given(@"I click login button")]
        [When(@"I click login button")]
        public void GivenIClickLoginButton()
        {
            this.homePage.ClickLoginButton();
        }

        [When(@"I enter ""(.*)"" and ""(.*)""")]
        public void WhenIEnterAnd(string email, string password)
        {
            this.loginPage.TypeCredentials(email, password);
        }

        [When(@"I submit login credentials")]
        public void WhenISubmitLoginCredentials()
        {
            this.loginPage.SubmitLoginCredentials();
        }

        [Then(@"I am logged to Trello")]
        public void ThenIAmLoggedToTrello()
        {
            Assert.IsTrue(this.mainPage.IsUserLogged());
        }

        [Then(@"I am not logged to Trello")]
        public void ThenIAmNotLoggedToTrello()
        {
            Assert.IsFalse(this.mainPage.IsUserLogged());
        }
    }
}
