﻿@login
Feature: Login into application
  As site user
  I want to log in
  In order to access site pages

  Background:
    Given I open Trello home page
    And I click login button

  @positive
  Scenario Outline: Successful login using correct credentials
    When I enter "<email>" and "<password>"
	And I submit login credentials
    Then I am logged to Trello

    Examples:
	| email                   | password   |
	| sirevis359@topmail2.net | Trello2019 |
	| casox90064@promailt.com | Trello2020 |

  @negative
  Scenario Outline: Try to login using correct email and incorrect password
    When I enter "<email>" and "<password>"
	And I submit login credentials
    Then I am not logged to Trello

    Examples:
	| email                   | password   |
	| sirevis359@topmail2.net | Trello2012 |
	| casox90064@promailt.com | Trello2030 |