﻿namespace ExamPlanner.Utilities
{
    using System.Collections.Generic;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using WebDriverManager.DriverConfigs.Impl;

    public static class SeleniumExecutor
    {
        public static IWebDriver webDriver;

        public static IWebDriver Driver => webDriver ?? (webDriver = CreateDriver());

        public static IWebDriver CreateDriver()
        {
            new WebDriverManager.DriverManager().SetUpDriver(new ChromeConfig());
            IWebDriver driver = new ChromeDriver(GetChromeOptions());
            return driver;
        }

        public static void Close()
        {
            if (webDriver != null)
            {
                Driver.Quit();
                webDriver = null;
            }
        }

        public static ChromeOptions GetChromeOptions()
        {
            Dictionary<string, object> chromePrefs = new Dictionary<string, object>();
            chromePrefs.Add("profile.default_content_settings.popups", 0);

            ChromeOptions options = new ChromeOptions();
            options.AddLocalStatePreference("prefs", chromePrefs);
            options.AddArguments("--disable-extensions");
            options.AcceptInsecureCertificates = true;

            return options;
        }
    }
}